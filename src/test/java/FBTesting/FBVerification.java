package FBTesting;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class FBVerification {
    WebDriver driver;


    @BeforeMethod
    void before() throws MalformedURLException {

        driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), DesiredCapabilities.chrome());
        driver.manage().window().maximize();
            }


    @Test
    void test() throws InterruptedException {
        driver.get("https://www.facebook.com");
        WebElement email = driver.findElement(By.id("email"));
        email.sendKeys("litstest@i.ua");
        WebElement password = driver.findElement(By.id("pass"));
        password.sendKeys("Linux-32");
        password.sendKeys(Keys.RETURN);
        WebElement welcomeSection = driver.findElement(By.id("pagelet_welcome"));
        Assert.assertTrue(welcomeSection.isDisplayed(), "Welcome section is not displayed");
    }


        //Add friends part
    void test2() {
        
//       Map<String, Object> prefs = new HashMap<String, Object>();
//       prefs.put("profile.default_content_setting_values.notifications", 2);
//       ChromeOptions options = new ChromeOptions();
//       options.setExperimentalOption("prefs", prefs);


        new WebDriverWait(driver, 60).until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[@id='fbRequestsJewel']/a"))));
        //driver.findElement(By.xpath("//div[@id='fbRequestsJewel']/a"));
        List<WebElement> possibleFriends = driver.findElements(By.xpath("//li[@class='friendBrowserListUnit']//div[@class='FriendButton']/button"));
        for (int i = 0; i <= 2; i++) {
            possibleFriends.get(i).click();
        }


    }
    @AfterMethod
    void after(){
        driver.quit();
    }
}


